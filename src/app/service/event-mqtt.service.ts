import { Injectable } from '@angular/core';
import { IMqttMessage, MqttService } from "ngx-mqtt";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class EventMqttService {

  private endpoint: string;

  constructor(
    private _mqttService: MqttService,
  ) {
    this.endpoint = 'events';
  }

  topic(): Observable<IMqttMessage> {
    let topicName = `forcar/asdas/10`;
    return this._mqttService.observe(topicName);
  }


  send(): Observable<void> {
    return this._mqttService.publish('forcar/asdas/10','Hello World Jovem Daniel');
  }
}
