import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { IMqttMessage, MqttService } from 'ngx-mqtt';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private _user_id;
  private _room;
  private _game;
  private _game_status;
  private _rooms;

  public subRoom: Subscription;
  public subChanel: Subscription;
  public avaibles: Subscription;

  public listener = false;

  constructor(
    private _mqttService: MqttService,
    private route: Router
    ) { }

    connectUserChannel(){
      return this._mqttService.observe(`user_channel/${this.user_id}`);
    }

    connectUserChanel(){
      return this._mqttService.observe(`user_chanel/${this.user_id}`);
    }

    startListener() {

      this.listener = true;

      this.subRoom = this.connectRoom().subscribe((message: IMqttMessage) => {
        let msg = JSON.parse(message.payload.toString())['py/state']
        this.game = msg;
        console.log("Room: ", msg);
        if(msg.event == "GAME_STARTED"){

          this.route.navigate(['/room/game'])
        }
      });
    }

    startListenerChanel(){
      console.log("iniciei o chanel")
      this.subChanel = this.connectUserChanel().subscribe((message: IMqttMessage) => {
        let msg = JSON.parse(message.payload.toString());
        this.game_status = msg;
        console.log('Channel: ', msg)
      })
    }

    avaibleGames(){
      console.log("iniciei o avaibles")
      this.avaibles =  this._mqttService.observe('available_rooms').subscribe( (message: IMqttMessage) => {
        let msg = JSON.parse(message.payload.toString());
        this.rooms = msg;
        console.log(msg)
      });
    }

    createRoom(){
      let user = {
        "client_id":this.user_id,
      }

      return this._mqttService.unsafePublish('create', JSON.stringify(user));
    }

    joinRoom(){
      let user = {
        "client_id":this.user_id
      }
      console.log("Joim room: ", user, this.room);
      return this._mqttService.unsafePublish(`room/${this.room}/join`, JSON.stringify(user));
    }

    startRoom(room: string, category: string){
      let user = {
        "client_id":this.user_id,
        "category":category
      }

      return this._mqttService.unsafePublish(`room/${room}/start`, JSON.stringify(user));
    }

    connectRoom(){
      return this._mqttService.observe(`room/${this.room}`);
    }

    leaveRoom(room){
      let user = {
        "client_id":this.user_id
      }
      this.subRoom.unsubscribe()
      return this._mqttService.unsafePublish(`room/${room}/leave`, JSON.stringify(user));
    }

    sendGuess(guess: string){
      let move = {
        "py/object":"none",
        "event":"GUESS",
        "guess": guess
      }

      return this._mqttService.unsafePublish(`user_channel/${this.user_id}`, JSON.stringify(move));
    }

    find(){
      console.log("Rodei o find");
      let user = {
        "client_id":this.user_id
      }

      return this._mqttService.unsafePublish('find', JSON.stringify(user));
    }


    exit(){
      console.log("exit")
      this.subChanel.unsubscribe();
      this.subRoom.unsubscribe();
      localStorage.clear()
      localStorage.removeItem('user_id');
      localStorage.removeItem('room');
      localStorage.removeItem('game');
      localStorage.removeItem('game_status');
    }

    public get user_id() {
      return localStorage.getItem('user_id');
    }

    public get room() {
      return localStorage.getItem('room');
    }
    public set room(value) {
      localStorage.setItem('room', value);
    }

    public get game() {
      return JSON.parse(localStorage.getItem('game'));
    }
    public set game(value) {
      localStorage.setItem('game', JSON.stringify(value));
    }

    public get game_status() {
      return JSON.parse(localStorage.getItem('game_status'));
    }
    public set game_status(value) {
      localStorage.setItem('game_status', JSON.stringify(value));
    }

    public get rooms() {
      return this._rooms;
    }
    public set rooms(value) {
      this._rooms = value;
    }
}
