import { Component, OnInit } from '@angular/core';
import { IMqttMessage } from 'ngx-mqtt';
import { Subscription } from 'rxjs';
import { GameService } from '../../service/game.service'

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {

  private user_channel: Subscription
  user_channel_msg;

  constructor(
    private game: GameService
  ) { }

  ngOnInit(): void {
    console.log("Entrei")
    this.user_channel = this.game.connectUserChannel().subscribe((message: IMqttMessage) => {
      this.user_channel_msg = JSON.parse(message.payload.toString());
      console.log(this.user_channel_msg);

      if(this.user_channel_msg.event == 'JOINED'){
        this.game.room = this.user_channel_msg.room;
        console.log("inicia listener")
        this.game.startListener();
      }

      if(this.user_channel_msg.event == "GUESS"){
        if(this.game.subChanel.closed){
          console.log("Iniciei o channel que tava fechado")
          this.game.startListenerChanel()
        }
        if(this.game.subRoom.closed){
          console.log("Iniciei o room que tava fechado")
          this.game.startListener()
        }
      }

    })
  }

}
