import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { IMqttMessage } from 'ngx-mqtt';
import { GameService } from 'src/app/service/game.service';

@Component({
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {

  public selectedItem;

  constructor(
    public game: GameService,
    private route: Router
  ) { }

  ngOnInit(): void {
    if(!this.game.room){
      console.log("criei a sala")
      this.game.createRoom();
    }else{
      console.log("entrei na sala")
      this.game.joinRoom();
    }
    this.game.startListener();
  }

  iniciarJogo(){
      console.log(this.selectedItem);
      this.game.startRoom(this.game.room, this.selectedItem);
  }

  sair(){
    this.game.leaveRoom(this.game.room);
    this.route.navigate([''])
  }
}
