import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { GameService } from 'src/app/service/game.service';

@Component({
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  public forcaControl = new FormControl();

  constructor(
    public game: GameService,
    private route: Router
  ) { }

  ngOnInit(): void {
    if(!this.game.listener){
      this.game.startListener();
    }
  }

  chutar(){
    let chute = this.forcaControl.value + ''
    this.forcaControl.setValue('');
    console.log(chute);
    this.game.sendGuess(chute.toUpperCase());
  }

  sair(){
    this.game.exit();
    this.route.navigate([''])
  }

}
