import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './components/game/game.component';
import { LobbyComponent } from './components/lobby/lobby.component';

import { RoomComponent } from './room.component';

const routes: Routes = [
  { path: '', component: RoomComponent ,
  children: [
    { path: 'lobby', component: LobbyComponent},
    { path: 'game', component: GameComponent},
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomRoutingModule { }
