import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from 'src/app/service/game.service';

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.scss']
})


export class FindComponent implements OnInit {

  displayedColumns: string[] = ['sala', 'qt', 'entrar'];

  constructor(
    public game: GameService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.game.find();
  }

  entrarSala(room: string){
    this.game.room = room;
    this.route.navigate(['room/lobby'])
  }

}
