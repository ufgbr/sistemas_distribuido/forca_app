import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FindRoutingModule } from './find-routing.module';
import { FindComponent } from './find.component';
import { MaterialModule } from 'src/app/material.module';


@NgModule({
  declarations: [FindComponent],
  imports: [
    CommonModule,
    FindRoutingModule,
    MaterialModule
  ]
})
export class FindModule { }
