import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GameService } from 'src/app/service/game.service';
import { EventMqttService } from '../../service/event-mqtt.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public loginControl = new FormControl('', [ Validators.required, Validators.minLength(2)]);
  public salaControl = new FormControl();

  constructor(
    private mqtt: EventMqttService,
    private route: Router,
    private game: GameService
  ) { }

  ngOnInit(): void {
    if(localStorage.getItem('user_id')){
      this.loginControl.setValue(localStorage.getItem('user_id'))
      localStorage.removeItem('room');
      localStorage.removeItem('game');
      localStorage.removeItem('game_status');
    }
  }

  criarSala(){
    if(this.loginControl.valid){
      localStorage.setItem('user_id', this.loginControl.value);
      this.game.startListenerChanel()
      this.route.navigate(['room/lobby']);
    }
  }

  procurarSala(){
    if(this.loginControl.valid){
      localStorage.setItem('user_id', this.loginControl.value);
      this.game.startListenerChanel()
      this.game.avaibleGames();
      this.route.navigate(['find']);
    }
  }

}
